#include "menu.hpp"

namespace menu {
    int menu_u (string menu_s) {
        int menu_vyber;

        cout << ODDELOVAC << menu_s << VYZVA;
        try{
            cin >> menu_vyber;
            return menu_vyber;
        } catch (std::exception const &e) {
            cerr << "Chyba vstupu " << e.what() << "\n";
            cin.clear();
            cin.ignore(numeric_limits<streamsize>::max(), '\n');
            return Err;
        }
    }

    int get_number(const string annouce) {
        int cislo;
        bool state = false;

        do{
            cout << annouce;
            try {
                state = true;
                cin >> cislo;
            } catch (std::exception ex) {
                cout << "Chyba vstupu: " << ex.what() << "\n";
                cin.clear();
                cin.ignore(numeric_limits<streamsize>::max(), '\n');
                state = false;
            }
        }while(!state);

        return cislo;
    }

    int get_number(const string annouce, int _min, int _max) {
        int cislo;
        bool state = false;

        do{
            cout << annouce;
            try {
                cin >> cislo;
                if(cislo >= _min && cislo <=_max) {
                    state = true;
                } else {
                    cout << "Chyba intervalu\n";
                }
            } catch (std::exception ex) {
                cout << "Chyba vstupu: " << ex.what() << "\n";
                cin.clear();
                cin.ignore(numeric_limits<streamsize>::max(), '\n');
                state = false;
            }
        }while(!state);

        return cislo;
    }
}
