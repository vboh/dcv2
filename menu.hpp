#ifndef MENU_HPP_INCLUDED
#define MENU_HPP_INCLUDED

#include <iostream>
#include <string>
#include <limits>

using namespace std;

namespace menu {
    const string VYZVA = "\nZadejte cislo volby: ";

    const string ODDELOVAC = "\n===============================\n";

    enum Menu_1_e {Err = -1, Novy = 1, Smazat, Vyhledat, Vypis, Export, Import, Konec};
    const string MENU_1_S = "MENU\n1\tNovy\n2\tSmazat\n3\tVyhledat\n4\tVypis\n5\tExport\n6\tImport\n7\tKonec";

    enum Menu_novy_e {Kniha_e = 1, Casopis_e};
    const string MENU_NOVY_S = "MENU/Novy:\n1\tKniha\n2\tCasopis";

    enum Menu_vypis_e {Vse_a_z = 1, Vse_z_a, Knihy_rok_ses, Knihy_autor_a_z, Casopisy_rok_ses};
    const string MENU_VYPIS_S = "MENU/Vypis:\n1\tVse A-Z\n2\tVse Z-A\n3\tKnihy dle roku vydani sestupne\n4\tKnihy dle autora A-Z\n5\tCasopisy sestupne dle roku";

    enum Menu_vyhledat_e {Vse_dle_nazvu = 1, Knihy_dle_autora};
    const string MENU_VYHLEDAT_S = "MENU/Vyhledat:\n1\tVse podle nazvu\n2\tKnihy podle autora";

    int menu_u (string menu_s);

    int get_number(const string annouce);
    int get_number(const string annouce, int _min, int _max);
}

#endif // MENU_HPP_INCLUDED
