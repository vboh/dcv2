#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <fstream>
#include <ctime>
#include <regex>

#include "kniha.hpp"
#include "casopis.hpp"
#include "menu.hpp"

using namespace std;
using namespace menu;

vector<tiskovina::Tiskovina*> tiskoviny;
vector<casopis::Casopis*> casopis_rok;
vector<kniha::Kniha*> kniha_rok;
vector<kniha::Kniha*> kniha_autor;

vector<tiskovina::Tiskovina*>::iterator uloz_knihu(kniha::Kniha* kniha) {
    using namespace kniha;

    vector<tiskovina::Tiskovina*>::iterator low_tis;
    low_tis = lower_bound(tiskoviny.begin(), tiskoviny.end(), kniha, tiskovina::CompareTiskovinaNazev() );
    vector<tiskovina::Tiskovina*>::iterator output = tiskoviny.insert(low_tis, kniha);

    vector<Kniha*>::iterator low_kni_rok;
    low_kni_rok = lower_bound(kniha_rok.begin(), kniha_rok.end(), kniha, CompareKnihaRok() );
    kniha_rok.insert(low_kni_rok, kniha);

    vector<Kniha*>::iterator low_kni_au;
    low_kni_au = lower_bound(kniha_autor.begin(), kniha_autor.end(), kniha, CompareKnihaAutor() );
    kniha_autor.insert(low_kni_au, kniha);

    return output;
}

vector<tiskovina::Tiskovina*>::iterator uloz_casopis(casopis::Casopis* casopis) {
    using namespace casopis;

    vector<tiskovina::Tiskovina*>::iterator low_tis;
    low_tis = lower_bound(tiskoviny.begin(), tiskoviny.end(), casopis, tiskovina::CompareTiskovinaNazev() );
    vector<tiskovina::Tiskovina*>::iterator output = tiskoviny.insert(low_tis, casopis);

    vector<Casopis*>::iterator low_cas;
    low_cas = lower_bound(casopis_rok.begin(), casopis_rok.end(), casopis, CompareCasopisRok() );
    casopis_rok.insert(low_cas, casopis);

    return output;
}

void novy() {
    Menu_novy_e choice = (Menu_novy_e)menu_u(MENU_NOVY_S);

    switch(choice) {
        case Kniha_e : {
            using namespace kniha;

            Kniha *kniha = new Kniha();

            cout << ODDELOVAC << "Nova kniha:\n";

            cout << "Nazev: ";
            cin >> kniha->nazev;

            cout << "Vydavatel: ";
            cin >> kniha->vydavatel;

            cout << "Zanr: ";
            cin >> kniha->zanr;

            cout << "Autor: ";
            cin >> kniha->autor;

            int rok = get_number("Rok vydani: ", 1900, 2100);
            kniha->rok_vydani = rok;

            time(&kniha->datum_vlozeni);

            (*uloz_knihu(kniha))->print();

            break;
        }

        case Casopis_e : {
            using namespace casopis;

            Casopis *casopis = new Casopis();

            cout << ODDELOVAC << "Novy casopis:\n";

            cout << "Nazev: ";
            cin >> casopis->nazev;

            cout << "Vydavatel: ";
            cin >> casopis->vydavatel;

            cout << "Zanr: ";
            cin >> casopis->zanr;

            int cislo = get_number("Rok vydani: ", 1900, 2100);
            casopis->rok = cislo;

            cislo = get_number("Cislo: ", 1, 2000);
            casopis->cislo = cislo;

            time(&casopis->datum_vlozeni);

            (*uloz_casopis(casopis))->print();

            break;
        }
    }

    cout << ODDELOVAC << "Pridana nova tiskovina:\n";
}

void vypis() {
    using namespace tiskovina;

    Menu_vypis_e choice = (Menu_vypis_e) menu_u(MENU_VYPIS_S);

    switch(choice) {
        case Vse_a_z : {
            cout << ODDELOVAC << "Vypis vsech zaznamu A-Z\n";
            for(vector<Tiskovina*>::iterator it = tiskoviny.begin(); it != tiskoviny.end(); ++it) {
                (*it)->print();
            }
            break;
        }

        case Vse_z_a : {
            cout << ODDELOVAC << "Vypis vsech zaznamu Z-A\n";
            for(vector<Tiskovina*>::reverse_iterator it = tiskoviny.rbegin(); it != tiskoviny.rend(); ++it) {
                (*it)->print();
            }
            break;
        }

        case Casopisy_rok_ses : {
            using namespace casopis;
            cout << ODDELOVAC << "Vypis casopisu podle roku sestupne\n";

            for(vector<Casopis*>::reverse_iterator it = casopis_rok.rbegin(); it != casopis_rok.rend(); ++it) {
                (*it)->print();
            }
            break;
        }

        case Knihy_rok_ses : {
            using namespace kniha;
            cout << ODDELOVAC << "Vypis knih podle roku vydani sestupne\n";

            for(vector<Kniha*>::reverse_iterator it = kniha_rok.rbegin(); it != kniha_rok.rend(); ++it) {
                (*it)->print();
            }
            break;
        }

        case Knihy_autor_a_z : {
            using namespace kniha;
            cout << ODDELOVAC << "Vypis knih podle roku vydani sestupne\n";

            for(vector<Kniha*>::iterator it = kniha_autor.begin(); it != kniha_autor.end(); ++it) {
                (*it)->print();
            }
            break;
        }
    }
};

vector<string> parse_csv_line(string line) {
    //Regulerni vyraz
    regex line_reg("\"?([a-zA-Z0-9 ,.:-]+)\"?;?");
    smatch line_match; //vysledek
    string::const_iterator line_it = line.cbegin(); //iterator - umoznuje projit string a postupne nalezt shodu
    vector<string> csv_line; //vektor s vyslednymi stringy

    while(regex_search(line_it, line.cend(), line_match, line_reg)) { //nactenenmi jednotlivych datovych polozek y radku CSV souboru
        line_it += line_match.str(0).size(); //posun ve stringu za aktualne nalezenou cast
        csv_line.insert(csv_line.end(), line_match.str(1));
    }
    return csv_line;
}

time_t parse_time(string time_s) {
    smatch time_m;
    time_t now;
    time(&now);
    tm *time = localtime(&now);

    regex_search(time_s, time_m, regex("([0-9]{1,2}).([0-9]{1,2}).([0-9]{4})-([0-9]{1,2}):([0-9]{1,2})"));
    if(time_m.size() == (5+1)) {
        try {
            time->tm_mday = stoi(time_m.str(1));
            time->tm_mon = stoi(time_m.str(2)) - 1;
            time->tm_year = stoi(time_m.str(3)) - 1900;
            time->tm_hour = stoi(time_m.str(4));
            time->tm_min = stoi(time_m.str(5));
        } catch (std::exception ex) {
            cout << "\n!!!ERROR: " << ex.what() << "\n";
        }
    } else {
        regex_search(time_s, time_m, regex("([0-9]{1,2}).([0-9]{1,2}).([0-9]{4})"));
        if(time_m.size() == (3+1)) {
            try{
                time->tm_mday = stoi(time_m.str(1));
                time->tm_mon = stoi(time_m.str(2)) - 1;
                time->tm_year = stoi(time_m.str(3)) - 1900;
            } catch (std::exception ex) {
                cout << "\n!!!ERROR: " << ex.what() << "\n";
            }
            time->tm_hour = 0;
            time->tm_min = 0;
        }
    }

    if(mktime(time) == -1) {
        return now;
    }

    return mktime(time);
}

void import() {
    cout << ODDELOVAC << "Import\n";

    int counter = 0;
    int line_counter = 0;
    string line;
    vector<string> csv_line;

{
    using namespace kniha;
    ifstream knihy;
    knihy.open("data/kni.csv");
    getline(knihy, line);

    while(getline(knihy, line)) {

        line_counter++;
        if(line_counter > 25) {
            cout << "|";
            line_counter = 0;
        }

        Kniha *kniha = new Kniha();

        csv_line = parse_csv_line(line);

        //vlozeni nalezenych datovych polozek do objektu
        kniha->nazev = csv_line.at(0);
        kniha->vydavatel = csv_line.at(1);
        kniha->zanr = csv_line.at(2);
        kniha->autor = csv_line.at(3);
        try {
            kniha->rok_vydani = stoi(csv_line.at(4));
        } catch (std::exception ex) {
            cout << "\n!!!ERROR: " << ex.what() << "\n";
        }
        kniha->datum_vlozeni = parse_time(csv_line.at(5));

        //kniha->print();
        //cout << "\n";

        uloz_knihu(kniha);

        counter++;
    }

    knihy.close();
}

{
    using namespace casopis;
    ifstream casopisy;
    casopisy.open("data/cas.csv");
    getline(casopisy, line);

    while(getline(casopisy, line)) {

        line_counter++;
        if(line_counter > 25) {
            cout << "|";
            line_counter = 0;
        }

        Casopis *casopis = new Casopis();

        csv_line = parse_csv_line(line);

        //vlozeni nalezenych datovych polozek do objektu
        casopis->nazev = csv_line.at(0);
        casopis->vydavatel = csv_line.at(1);
        casopis->zanr = csv_line.at(2);
        try {
            casopis->rok = stoi(csv_line.at(3));
            casopis->cislo = stoi(csv_line.at(4));
        } catch (std::exception ex) {
            cout << "\n!!!ERROR: " << ex.what();
        }
        casopis->datum_vlozeni = parse_time(csv_line.at(5));

        //casopis->print();
        //cout << "\n";

        uloz_casopis(casopis);

        counter++;
    }

    casopisy.close();
}

    cout << "\nImportovano " << counter << " zaznamu\n";

}

void exportuj() {
{
    using namespace kniha;
    ofstream knihy;
    knihy.open("data/kni.csv");

    knihy << "Nazev;Vydavatel;Zanr;Autor;Rok_vydani;Datum_vlozeni\n";

    for(vector<Kniha*>::iterator it = kniha_rok.begin(); it != kniha_rok.end(); ++it) {
        (*it)->print_csv(knihy);
    }

    knihy.close();
}

{
    using namespace casopis;
    ofstream casopisy;
    casopisy.open("data/cas.csv");

    casopisy << "Nazev;Vydavatel;Zanr;Rok;Cislo;Datum_vlozeni\n";

    for(vector<Casopis*>::iterator it = casopis_rok.begin(); it != casopis_rok.end(); ++it) {
        (*it)->print_csv(casopisy);
    }

    casopisy.close();
}
}

template <class T>
struct bounds_returned {
    typename vector<T*>::iterator low;
    typename vector<T*>::iterator upp;
};

template <class Output, class T>
Output find_bounds(typename vector<T*>::iterator low, typename vector<T*>::iterator upp, string hledane, unsigned int max_level, unsigned int what = 0, unsigned int level = 0) {

    typename vector<T*>::iterator low_n = lower_bound(low, upp, hledane, typename T::CompareRecursive(level, what));
    typename vector<T*>::iterator upp_n = upper_bound(low, upp, hledane, typename T::CompareRecursive(level, what));

    level++;
    if(level < max_level) {
        return find_bounds<Output, T>(low_n, upp_n, hledane, max_level, what, level);
    }

    bounds_returned<T> out;
    out.low = low_n;
    out.upp = upp_n;
    return out;
}

void vyhledat() {
    Menu_vyhledat_e choice = (Menu_vyhledat_e)menu_u(MENU_VYHLEDAT_S);

    switch (choice) {
        case Vse_dle_nazvu : {
            using namespace tiskovina;

            cout << "Zadejte max. 4 pocatecni pismena nazvu: ";
            string hledane;
            cin >> hledane;

            bounds_returned<tiskovina::Tiskovina> vysledek = find_bounds<bounds_returned<Tiskovina>, tiskovina::Tiskovina>
            (tiskoviny.begin(), tiskoviny.end(), hledane, ((hledane.size() <= 4) ? hledane.size() : 4));

            cout << ODDELOVAC << "Nalezeno " << (vysledek.upp - vysledek.low) << " zaznamu\n";
            for(vector<Tiskovina*>::iterator it = vysledek.low; it != vysledek.upp; ++it) {
                (*it)->print();
            }

            break;
        }

        case Knihy_dle_autora : {
            using namespace kniha;

            cout << "Zadejte max. 4 pocatecni pismena jmena autora: ";
            string hledane;
            cin >> hledane;

            bounds_returned<kniha::Kniha> vysledek = find_bounds<bounds_returned<kniha::Kniha>, kniha::Kniha>
            (kniha_autor.begin(), kniha_autor.end(), hledane, ((hledane.size() <= 4) ? hledane.size() : 4));

            cout << ODDELOVAC << "Nalezeno " << (vysledek.upp - vysledek.low) << " zaznamu\n";
            for(vector<Kniha*>::iterator it = vysledek.low; it != vysledek.upp; ++it) {
                (*it)->print();
            }

            break;
        }
    }
}

void smazat() {
    using namespace tiskovina;

    cout << "Zadejte max. 4 pocatecni pismena nazvu: ";
    string hledane;
    cin >> hledane;

    bounds_returned<tiskovina::Tiskovina> vysledek = find_bounds<bounds_returned<Tiskovina>, tiskovina::Tiskovina>
            (tiskoviny.begin(), tiskoviny.end(), hledane, ((hledane.size() <= 4) ? hledane.size() : 4));

    cout << ODDELOVAC << "Nalezeno " << (vysledek.upp - vysledek.low) << " zaznamu\n\n";

    if((vysledek.upp - vysledek.low) != 0) {

        for(vector<Tiskovina*>::iterator it = vysledek.low; it != vysledek.upp; ++it) {
            cout << (it - vysledek.low) << " > ";
            (*it)->print();
        }

        int cislo = get_number("Zadejte cislo tiskoviny ke smazani: ", 0, (vysledek.upp - vysledek.low) - 1);

        vysledek.low += cislo;
        cout << "\n";

        Tiskovina *k_smazani = (*vysledek.low);
        k_smazani->print();

        string souhlas;
        do{
            cout << "Smazat tuto tiskovinu? (A/n)\n";
            cin >> souhlas;
        }while (!(souhlas == "A" || souhlas == "n"));

        if(souhlas == "A") {
            tiskoviny.erase(vysledek.low);

            auto it = find(kniha_autor.begin(), kniha_autor.end(), k_smazani);
            if (it != kniha_autor.end()) {
                kniha_autor.erase(it);
            }

            it = find(kniha_rok.begin(), kniha_rok.end(), k_smazani);
            if (it != kniha_rok.end()) {
                kniha_rok.erase(it);
            }

            auto it2 = find(casopis_rok.begin(), casopis_rok.end(), k_smazani);
            if (it2 != casopis_rok.end()) {
                casopis_rok.erase(it2);
            }

            delete(k_smazani);

            cout << "Smazano\n";
        }
    }
}

int main() {
    //Nastaveni exceptions bits
    cin.exceptions(iostream::failbit | iostream::badbit);

    bool running = true;
    Menu_1_e choice = Err;

    while(running) {

        choice = (Menu_1_e)menu_u(MENU_1_S);
        switch (choice) {
            case Novy : {
                novy();
                break;
            }

            case Vypis : {
                vypis();
                break;
            }

            case Import : {
                import();
                break;
            }

            case Export : {
                exportuj();
                break;
            }

            case Vyhledat : {
                vyhledat();
                break;
            }

            case Smazat : {
                smazat();
                break;
            }

            default : {break;}

            case Konec : {
                for(vector<tiskovina::Tiskovina*>::iterator iter = tiskoviny.begin(); iter != tiskoviny.end(); ++iter) {
                    tiskovina::Tiskovina *k_smazani = (*iter);

                    tiskoviny.erase(iter);

                    auto it = find(kniha_autor.begin(), kniha_autor.end(), k_smazani);
                    if (it != kniha_autor.end()) {
                        kniha_autor.erase(it);
                    }

                    it = find(kniha_rok.begin(), kniha_rok.end(), k_smazani);
                    if (it != kniha_rok.end()) {
                        kniha_rok.erase(it);
                    }

                    auto it2 = find(casopis_rok.begin(), casopis_rok.end(), k_smazani);
                    if (it2 != casopis_rok.end()) {
                        casopis_rok.erase(it2);
                    }

                    delete(k_smazani);
                }

                running = false;
                break;
            }
        }

    }
    return 0;
}













