﻿# Domácí úkol #2
Jsem rád, že jste sem zavítal, na tomto úkolu jsem si dal obzvlášť záležet. Sice jsem nevytvořil celou DB jako objekt, ale mám jiné vychytávky.

* rekurzivní vyhledávání podle až 4 znaků pomocí funkcí z knihovny algorithm
    * funkce rekurzivního vyhledávání využívá template, aby ji bylo možné použít pro více typů vektorů
* parsování CSV pomocí regex
* menu skrz univerzální funkci s využitím enum
* řazení už při ukládání, opět pomocí funkcí z knihovny algorithm


Doufám, že vás moje troufalost neurazila, ale docela mě i moje kamarády zajímalo jak to dopadne :D