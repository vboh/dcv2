#ifndef CASOPIS_HPP_INCLUDED
#define CASOPIS_HPP_INCLUDED

#include "tiskovina.hpp"

namespace casopis {

class Casopis : public tiskovina::Tiskovina {
    public:
    int rok;
    int cislo;

    void print();

    void print_csv(std::ofstream &os);

    ~Casopis() {};
};

struct CompareCasopisRok {

    bool operator() (const Casopis *left, const Casopis *right) const {
        return left->rok < right->rok;
    }

};

}

#endif // CASOPIS_HPP_INCLUDED
