#include "kniha.hpp"

namespace kniha {

    void Kniha::print() {
        tm *cas = localtime(&this->datum_vlozeni);
        cout << "Kniha; "
        << "Nazev: " << this->nazev
        << "; Vydavatel: " << this->vydavatel
        << "; Zanr: " << this->zanr
        << "; Autor: " << this->autor
        << "; Rok vydani: " << this->rok_vydani
        << "; Datum vlozeni: "
        << cas->tm_mday << "." << (cas->tm_mon + 1) << "." << (cas->tm_year + 1900)
        << "-" << cas->tm_hour << ":" << cas->tm_min << std::endl;
    }

    void Kniha::print_csv(std::ofstream &os) {
        tm *cas = localtime(&this->datum_vlozeni);
        os << this->nazev << ";"
        << this->vydavatel << ";"
        << this->zanr << ";"
        << this->autor << ";"
        << this->rok_vydani << ";"
        << cas->tm_mday << "." << (cas->tm_mon + 1) << "." << (cas->tm_year + 1900)
        << "-" << cas->tm_hour << ":" << cas->tm_min << std::endl;
    }
}
