#include "import.hpp"

vector<string> parse_csv_line(string line) {
    //Regulerni vyraz
    regex line_reg("\"?([a-zA-Z0-9 ,.:-]+)\"?;?");
    smatch line_match; //vysledek
    string::const_iterator line_it = line.cbegin(); //iterator - umoznuje projit string a postupne nalezt shodu
    vector<string> csv_line; //vektor s vyslednymi stringy

    while(regex_search(line_it, line.cend(), line_match, line_reg)) { //nactenenmi jednotlivych datovych polozek y radku CSV souboru
        line_it += line_match.str(0).size(); //posun ve stringu za aktualne nalezenou cast
        csv_line.insert(csv_line.end(), line_match.str(1));
    }
    return csv_line;
}

time_t parse_time(string time_s) {
    smatch time_m;
    time_t now;
    time(&now);
    tm *time = localtime(&now);

    regex_search(time_s, time_m, regex("([0-9]{1,2}).([0-9]{1,2}).([0-9]{4})-([0-9]{1,2}):([0-9]{1,2})"));
    if(time_m.size() == (5+1)) {
        time->tm_mday = stoi(time_m.str(1));
        time->tm_mon = stoi(time_m.str(2)) - 1;
        time->tm_year = stoi(time_m.str(3)) - 1900;
        time->tm_hour = stoi(time_m.str(4));
        time->tm_min = stoi(time_m.str(5));
    } else {
        regex_search(time_s, time_m, regex("([0-9]{1,2}).([0-9]{1,2}).([0-9]{4})"));
        if(time_m.size() == (3+1)) {
            time->tm_mday = stoi(time_m.str(1));
            time->tm_mon = stoi(time_m.str(2)) - 1;
            time->tm_year = stoi(time_m.str(3)) - 1900;
            time->tm_hour = 0;
            time->tm_min = 0;
        }
    }

    return mktime(time);
}

void import() {
    cout << ODDELOVAC << "Import\n";

{
    using namespace kniha;
    ifstream knihy;
    knihy.open("data/kni.csv");

    string line;
    vector<string> csv_line;
    getline(knihy, line);


    while(getline(knihy, line)) {

        Kniha *kniha = new Kniha();

        csv_line = parse_csv_line(line);

        //vlozeni nalezenych datovych polozek do objektu
        kniha->nazev = csv_line.at(0);
        kniha->vydavatel = csv_line.at(1);
        kniha->zanr = csv_line.at(2);
        kniha->autor = csv_line.at(3);
        kniha->rok_vydani = stoi(csv_line.at(4));
        kniha->datum_vlozeni = parse_time(csv_line.at(5));

        kniha->print();
        cout << "\n";
    }

    knihy.close();
}

}
