#ifndef TISKOVINA_HPP_INCLUDED
#define TISKOVINA_HPP_INCLUDED

#include <iostream>
#include <string>
#include <ctime>
#include <regex>
#include <fstream>

using namespace std;

namespace tiskovina {

class Tiskovina {
    public:
    string nazev;
    string vydavatel;
    time_t datum_vlozeni;
    string zanr;

    virtual void print() = 0;
    virtual void print_csv(std::ofstream &os) = 0;

    virtual ~Tiskovina() {};

    struct CompareRecursive {
        unsigned int uroven;
        unsigned int what;

        CompareRecursive(unsigned int uroven, unsigned int what) {
            this->uroven = uroven;
            this->what = what;
        }

        bool operator() (const Tiskovina *left, const string &right) const {
            switch (what) {
                case (0) : {
                    if(left->nazev.size() > uroven && right.size() > uroven) {
                        return left->nazev[uroven] < right[uroven];
                    }
                    return false;
                }
            }
            return false;
        }

        bool operator() (const string &left, const Tiskovina *right) const {
            switch (what) {
                case (0) : {
                    if(right->nazev.size() > uroven && left.size() > uroven) {
                        return left[uroven] < right->nazev[uroven];
                    }
                    return false;
                }
            }
            return false;
        }
    };
};

struct CompareTiskovinaNazev {

    bool operator() (const Tiskovina *left, const Tiskovina *right) const {
        return left->nazev < right->nazev;
    }

    bool operator() (const Tiskovina *left, const string &right) const {
        return left->nazev < right;
    }

    bool operator() (const string &left, const Tiskovina *right) const {
        return left < right->nazev;
    }

};



}
#endif // TISKOVINA_HPP_INCLUDED
