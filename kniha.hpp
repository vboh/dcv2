#ifndef KNIHA_HPP_INCLUDED
#define KNIHA_HPP_INCLUDED

#include "tiskovina.hpp"

namespace kniha {
class Kniha : public tiskovina::Tiskovina {
    public:
    string autor;
    int rok_vydani;

    void print();

    void print_csv(std::ofstream &os);

    string string_csv();

    ~Kniha() {};

    struct CompareRecursive {
        unsigned int uroven;
        unsigned int what;

        CompareRecursive(unsigned int uroven, unsigned int what) {
            this->uroven = uroven;
            this->what = what;
        }

        bool operator() (const Kniha *left, const string &right) const {
            switch (what) {
                case (0) : {
                    if(left->nazev.size() > uroven && right.size() > uroven) {
                        return left->autor[uroven] < right[uroven];
                    }
                    return false;
                }
            }
            return false;
        }

        bool operator() (const string &left, const Kniha *right) const {
            switch (what) {
                case (0) : {
                    if(right->autor.size() > uroven && left.size() > uroven) {
                        return left[uroven] < right->autor[uroven];
                    }
                    return false;
                }
            }
            return false;
        }
    };
};

struct CompareKnihaRok {

    bool operator() (const Kniha *left, const Kniha *right) const {
        return left->rok_vydani < right->rok_vydani;
    }

};

struct CompareKnihaAutor {

    bool operator() (const Kniha *left, const Kniha *right) const {
        return left->autor < right->autor;
    }

};

}

#endif // KNIHA_HPP_INCLUDED
