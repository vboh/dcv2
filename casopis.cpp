#include "casopis.hpp"

namespace casopis {

    void Casopis::print() {
        tm *cas = localtime(&this->datum_vlozeni);
        cout << "Casopis; "
        << "Nazev: " << this->nazev
        << "; Vydavatel: " << this->vydavatel
        << "; Zanr: " << this->zanr
        << "; Rok: " << this->rok
        << "; Cislo: " << this->cislo
        << "; Datum vlozeni: "
        << cas->tm_mday << "." << (cas->tm_mon + 1) << "." << (cas->tm_year + 1900)
        << "-" << cas->tm_hour << ":" << cas->tm_min << std::endl;
    }

    void Casopis::print_csv(std::ofstream &os) {
        tm *cas = localtime(&this->datum_vlozeni);
        os << this->nazev << ";"
        << this->vydavatel << ";"
        << this->zanr << ";"
        << this->rok << ";"
        << this->cislo << ";"
        << cas->tm_mday << "." << (cas->tm_mon + 1) << "." << (cas->tm_year + 1900)
        << "-" << cas->tm_hour << ":" << cas->tm_min << std::endl;
    }
}
